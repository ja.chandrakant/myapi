const express =  require('express');
const app = express();


function auth(req, res, next) {
   //check for req headers

   if(req && req.headers) {
      if(!req.headers.authorization) {
         return res.status(401).json({error: 'Auth failure'})
      } else {
         const auth = req.headers.authorization.split(' ')[1];
         const creds = Buffer.from(auth, 'base64').toString('ascii');
         if(!creds.match(/testUser/)) {
            return res.status(401).json({error: 'Auth failure'})
         }
      }

   }

   next();
}

app.use(auth)

app.get('/user', (req, res) => {

   // res.json(user);
   res.json({name: "test", age: 12})
});



app.listen(8888, () => {
   console.log('Server listening on port 8888.');
});
